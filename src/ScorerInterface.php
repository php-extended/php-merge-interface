<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Record\AssignableRecordProviderInterface;
use PhpExtended\Record\RecordProviderInterface;
use Stringable;

/**
 * ScorerInterface interface file.
 * 
 * This interface specifies an engine to compare source data to challenger
 * data in order to create scores for the challenger against the source.
 * 
 * @author Anastaszor
 */
interface ScorerInterface extends Stringable
{
	
	/**
	 * Generates the score list from a source record provider and the challenger
	 * assignable record provider and the definition list interface.
	 * 
	 * @param RecordProviderInterface $source
	 * @param string $srcNamespace the source namespace
	 * @param string $srcClassname the source classname
	 * @param AssignableRecordProviderInterface $challenger
	 * @param string $chlNamespace the challenger namespace
	 * @param string $chlClassname the challenger classname
	 * @param array<integer, ScoreCalculationDefinitionInterface> $calcDefList
	 * @return array<integer, ScoreResultInterface>
	 */
	public function calculateScore(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		AssignableRecordProviderInterface $challenger,
		string $chlNamespace,
		string $chlClassname,
		array $calcDefList
	) : array;
	
}
