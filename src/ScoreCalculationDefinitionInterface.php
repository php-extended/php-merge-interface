<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Score\ScoreCollectionFactoryInterface;
use PhpExtended\Score\ScoreFactoryInterface;
use PhpExtended\Score\ScorePolicyFactoryInterface;
use Stringable;

/**
 * ScoreCalculationDefinitionInterface interface file.
 * 
 * This interface represents a definition of the elements to create a score 
 * from informations that challengers give to match about a source.
 * 
 * @author Anastaszor
 */
interface ScoreCalculationDefinitionInterface extends Stringable
{
	
	/**
	 * Gets the name of the field that should be used in the source information
	 * objects given by the source information provider.
	 * 
	 * @return string
	 */
	public function getSourceFieldname() : string;
	
	/**
	 * Gets the name of the field that should be used in the challenger 
	 * information objects given by the challenger information provider.
	 * 
	 * @return string
	 */
	public function getChallengerFieldname() : string;
	
	/**
	 * Gets the score collection factory that will create the score collection
	 * objects that will be used in the calculation.
	 * 
	 * @return ScoreCollectionFactoryInterface
	 */
	public function getScoreCollectionFactory() : ScoreCollectionFactoryInterface;
	
	/**
	 * Gets the score factory that will create the score objects that will be
	 * used in the calculation.
	 * 
	 * @return ScoreFactoryInterface
	 */
	public function getScoreFactory() : ScoreFactoryInterface;
	
	/**
	 * Gets the score policy factory that will create the score policy objects
	 * that will be used in the calculation.
	 * 
	 * @return ScorePolicyFactoryInterface
	 */
	public function getScorePolicyFactory() : ScorePolicyFactoryInterface;
	
}
