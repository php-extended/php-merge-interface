<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Information\InformationInterface;
use PhpExtended\Record\AssignableRecordProviderInterface;
use PhpExtended\Record\RecordProviderInterface;
use Stringable;

/**
 * MergeInterface interface file.
 * 
 * This interface represents the merging process of multiple objects into 
 * single informations.
 * 
 * @author Anastaszor
 */
interface MergerInterface extends Stringable
{
	
	/**
	 * Creates the informations that provides from the merging of the challenger
	 * information that matches the source data.
	 * 
	 * @param RecordProviderInterface $source
	 * @param string $srcNamespace the source namespace
	 * @param string $srcClassname the source classname
	 * @param array<integer, AssignableRecordProviderInterface> $challengerList
	 * @param string $chlNamespace the challenger namespace
	 * @param string $chlClassname the challenger classname
	 * @param array<integer, MergeCalculationDefinitionInterface> $calcDefList
	 * @return array<integer, InformationInterface>
	 */
	public function doMerge(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		array $challengerList,
		string $chlNamespace,
		string $chlClassname,
		array $calcDefList
	) : array;
	
}
