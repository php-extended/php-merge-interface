<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * ScoreResultInterface interface file.
 * 
 * This interface represents a score and its environment variables.
 * 
 * @author Anastaszor
 */
interface ScoreResultInterface extends Stringable
{
	
	/**
	 * The namespace as the result.
	 * 
	 * @return string
	 */
	public function getNamespace() : string;
	
	/**
	 * The classname used as the result.
	 * 
	 * @return string
	 */
	public function getClassname() : string;
	
	/**
	 * The fieldname used as the result.
	 * 
	 * @return string
	 */
	public function getFieldname() : string;
	
	/**
	 * The score that was given to this module, class and field.
	 * 
	 * @return ScoreInterface
	 */
	public function getScore() : ScoreInterface;
	
}
