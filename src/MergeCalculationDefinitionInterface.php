<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Vote\BiasFactoryInterface;
use PhpExtended\Vote\CandidateFactoryInterface;
use PhpExtended\Vote\CitizenFactoryInterface;
use PhpExtended\Vote\ElectionRunnerFactoryInterface;
use PhpExtended\Vote\VotingMethodFactoryInterface;
use Stringable;

/**
 * MergeCalculationDefinitionInterface interface file.
 * 
 * This interface represents a definition of the elements to create a merged
 * information from informations that challengers give for a specific source.
 * 
 * @author Anastaszor
 */
interface MergeCalculationDefinitionInterface extends Stringable
{
	
	/**
	 * Gets the name of the field that should be used in the source information
	 * objects given by the source information provider.
	 * 
	 * @return string
	 */
	public function getSourceFieldName() : string;
	
	/**
	 * Gets the name of the field that should be used in the challenger 
	 * information objects given by the challenger information providers.
	 * 
	 * @return string
	 */
	public function getChallengerFieldName() : string;
	
	/**
	 * Gets the voting method factory that will create voting methods that will
	 * be used in the election process.
	 * 
	 * @return VotingMethodFactoryInterface
	 */
	public function getVotingMethodFactory() : VotingMethodFactoryInterface;
	
	/**
	 * Gets the citizen factory that will create the citizen objects for each
	 * vote in the election process.
	 * 
	 * @return CitizenFactoryInterface
	 */
	public function getCitizenFactory() : CitizenFactoryInterface;
	
	/**
	 * Gets the candidate factory that will create the candidate objects for
	 * each vote in the election process.
	 * 
	 * @return CandidateFactoryInterface
	 */
	public function getCandidateFactory() : CandidateFactoryInterface;
	
	/**
	 * Gets the bias factory that will create the bias objects used in the
	 * election process.
	 * 
	 * @return BiasFactoryInterface
	 */
	public function getBiasFactory() : BiasFactoryInterface;
	
	/**
	 * Gets the election runner factory that will create the election runner
	 * objects that will be used to run the election process.
	 * 
	 * @return ElectionRunnerFactoryInterface
	 */
	public function getElectionRunnerFactory() : ElectionRunnerFactoryInterface;
	
}
