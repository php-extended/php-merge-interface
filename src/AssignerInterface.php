<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-merge-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Merge;

use PhpExtended\Record\AssignableRecordProviderInterface;
use PhpExtended\Record\RecordComparatorInterface;
use PhpExtended\Record\RecordProviderInterface;
use RuntimeException;
use Stringable;

/**
 * AssignerInterface interface file.
 * 
 * This class assigns the records from the challenger record provider with the
 * records of the source record provider.
 * 
 * @author Anastaszor
 */
interface AssignerInterface extends Stringable
{
	
	/**
	 * Assigns all the records from the challenger assignable record provider
	 * with the records from the source record provider.
	 * 
	 * @param RecordProviderInterface $source
	 * @param string $srcNamespace the source namespace
	 * @param string $srcClassname the source classname
	 * @param AssignableRecordProviderInterface $challenger
	 * @param string $chlNamespace the challenger namespace
	 * @param string $chlClassname the challenger classname
	 * @param RecordComparatorInterface $comparator
	 * @return integer the number of successful assignments
	 * @throws RuntimeException one of the assignment fails
	 */
	public function doAssignments(
		RecordProviderInterface $source,
		string $srcNamespace,
		string $srcClassname,
		AssignableRecordProviderInterface $challenger,
		string $chlNamespace,
		string $chlClassname,
		RecordComparatorInterface $comparator
	) : int;
	
}
